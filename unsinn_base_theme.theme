<?php

use Drupal\Core\Render\Element;

/**
 * Implements template_preprocess_html().
 */
function unsinn_base_theme_preprocess_html(array &$variables) {
  if (\Drupal::service('path.matcher')->isFrontPage()) {
    $variables['attributes']['class'][] = 'is-front';
  }
  else {
    $variables['attributes']['class'][] = 'is-not-front';
  }

  $theme_name = \Drupal::service('theme.manager')->getActiveTheme()->getName();
  $banner_block_name = sprintf('%s_banner', $theme_name);

  // Check, if we have an active banner block on the page's teaser region.
  if (!empty($variables['page']['teaser'])) {
    $blocks = Element::children($variables['page']['teaser']);
    foreach ($blocks as $block) {
      if (strpos($block, $banner_block_name) === 0) {
        // Check the banner block for emptiness.
        /** @var \Drupal\banner\BannerFinderInterface $banner_finder */
        $banner_finder = \Drupal::service('banner.banner_finder');
        $bannerIds = $banner_finder->getBannerIdsFromActiveNode();
        if (!empty($bannerIds)) {
          $variables['attributes']['class'][] = 'has-page-banner';
        }
        break;
      }
    }
  }

  if (!empty($variables['node_type'])) {
    // On node pages, add the node type to the body classes.
    $variables['attributes']['class'][] = 'page-node';
    $variables['attributes']['class'][] = 'page-node--' . $variables['node_type'];
  }

  $route_name = \Drupal::routeMatch()->getRouteName();
  if ($route_name == 'system.404') {
    $variables['attributes']['class'][] = 'page--http--404';
  }
  elseif ($route_name == 'unsm_finder.trailer.details') {
    $variables['attributes']['class'][] = 'remote-trailer-detail-page';
  }
}

/**
 * Implements template_preprocess_page().
 */
function unsinn_base_theme_preprocess_page(array &$variables) {
  $current_route = \Drupal::routeMatch()->getRouteName();
  $use_big_sidebar = in_array($current_route, [
    'unsm_finder.finder.result',
    'unsm_finder.trailer.details'
  ]);
  $variables['use_big_sidebar'] = $use_big_sidebar;
  $variables['has_sidebar'] = $use_big_sidebar || !empty($variables['page']['sidebar']);
  $variables['has_banner'] = !empty($variables['page']['teaser']);

  /** @var \Drupal\agorasocial\SocialLinkRendererInterface $social_link_renderer */
  $social_link_renderer = \Drupal::service('agorasocial.social_link_renderer');
  $variables['social_links'] = $social_link_renderer->renderSocialLinks();

  /** @var \Drupal\site_settings\SiteSettingsLoader $site_settings */
  $site_settings = \Drupal::service('site_settings.loader');
  $settings = $site_settings->loadByFieldset('Öffnungszeiten');
  if ($settings && !empty($settings['opening_hours'])) {
    $variables['opening_hours'] = check_markup($settings['opening_hours']['value'], $settings['opening_hours']['format']);
  }
}

/**
 * Implements template_preprocess_block().
 */
function unsinn_base_theme_preprocess_block(array &$variables) {
  $plugin_id = $variables['elements']['#plugin_id'] ?? '';
  $base_plugin_id = $variables['elements']['#base_plugin_id'] ?? '';
  // We do not have the region information here, but we know the blocks there.
  $is_footer_block = in_array($plugin_id, [
    'agoralocation_location',
    'sendinblue_iframe_customer',
  ]);
  if (!$is_footer_block && $base_plugin_id === 'block_content' && $variables['label'] === 'Öffnungszeiten') {
    $is_footer_block = TRUE;
  }
  $variables['is_footer_block'] = $is_footer_block;
}

/**
 * Implements template_preprocess_node().
 */
function unsinn_base_theme_preprocess_node(array &$variables) {
  /** @var \Drupal\node\NodeInterface $node */
  $node = $variables['node'];
  switch ($node->bundle()) {
    case 'news':
      if ($variables['view_mode'] == 'full') {
        $variables['overview_url'] = agoranews_get_news_overview_url();
      }
      else {
        $variables['created'] = \Drupal::service('date.formatter')->format($node->getCreatedTime(), 'custom', 'd.m.Y');
      }
      break;

    case 'reference':
      if ($variables['view_mode'] == 'full') {
        $variables['overview_url'] = unsm_references_get_reference_overview_url();
      }
      break;
  }

  if ($variables['view_mode'] === 'full') {
    $variables['has_body'] = $node->hasField('body') && !$node->get('body')->isEmpty();
  }
}

/**
 * Implements template_preprocess_input().
 */
function unsinn_base_theme_preprocess_input(array &$variables) {
  if ($variables['theme_hook_original'] != 'input__submit') {
    return;
  }

  // Set flag for Twig template, whether or not we should use a button element.
  $use_button = FALSE;
  if (!empty($variables['attributes']['data-use-button'])) {
    unset($variables['attributes']['data-use-button']);
    $use_button = TRUE;
  }
  $variables['use_button'] = $use_button;

  // Set flag for Twig template, whether or not we should omit printing the
  // button text. This will only be done, when an icon is used below.
  $omit_text = FALSE;
  if (!empty($variables['attributes']['data-icon'])) {
    $variables['icon'] = 'far fa-' . $variables['attributes']['data-icon'];
    unset($variables['attributes']['data-icon']);
    if (!empty($variables['attributes']['data-omit-text'])) {
      unset($variables['attributes']['data-omit-text']);
      $omit_text = TRUE;
    }
  }
  $variables['omit_text'] = $omit_text;
}

/**
 * Implements template_preprocess_remote_gallery().
 */
function unsinn_base_theme_preprocess_remote_gallery(array &$variables) {
  if (\Drupal::service('module_handler')->moduleExists('photoswipe')) {
    \Drupal::service('photoswipe.assets_manager')->attach($variables);
  }
}

/**
 * Implements template_preprocess_remote_trailer().
 */
function unsinn_base_theme_preprocess_remote_trailer(array &$variables) {
  if (!empty($variables['tabs']['downloads'])) {
    $variables['tabs']['downloads']['value']['#theme_wrappers']['container']['#attributes']['class'] = ['remote-downloads', 'grid-x', 'grid-padding-x', 'small-up-2', 'medium-up-2', 'large-up-3'];
  }
}

/**
 * Implements template_preprocess_email_wrap().
 */
function unsinn_base_theme_preprocess_email_wrap(array &$variables) {
  // Deactivate cache at all.
  $variables['#cache']['max-age'] = 0;

  // @todo Find a generic way to add a signature (theme hook + settings).
//  $variables['signature'] = '';

  // @todo Here we could add the site logo (or an e-mail specific version of it).
//  $site_logo = [
//    '#theme' => 'image',
//    '#uri' => 'assets/img/email/logo.png',
//    '#alt' => '',
//  ];
//
//  $renderer = \Drupal::service('renderer');
//  $absolute_url_prefix = \Drupal::request()->getSchemeAndHttpHost() . '/';
//  /*
//   * I absolutely don't like this solution, as it is quite hacky. But I
//   * haven't found a better way to:
//   * change the output of the image 'src' attribute and replace it with an
//   * absolute url, as swiftmailer needs this in order to correctly inline the image.
//   */
//  $rendered_output = $renderer->render($site_logo);
//  $variables['site_logo'] = ['#markup' => str_replace('src="/', 'src="' . $absolute_url_prefix, $rendered_output)];
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function unsinn_base_theme_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  $route_name = \Drupal::routeMatch()->getRouteName();
  switch ($route_name) {
//    case 'system.401':
//      // Unauthorized Access.
//      $error = 401;
//      break;
//
//    case 'system.403':
//      // Access Denied.
//      $error = 403;
//      break;

    case 'system.404':
      // Page Not Found.
      $error = 404;
      break;
  }
  if (isset($error)) {
    $suggestions[] = 'page__' . $error;
  }
}
