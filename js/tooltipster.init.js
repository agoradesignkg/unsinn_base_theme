/**
 * @file
 * Javascript behaviors for Tooltipster integration.
 */

(function ($, Drupal) {

  'use strict';

  if (!$.fn.tooltipster) {
    return;
  }

  $('.tooltipster').tooltipster({
    contentCloning: true
  });

})(jQuery, Drupal);